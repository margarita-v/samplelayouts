package com.margarita.samplelayouts;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    @BindView(R.id.ip)
    EditText mIpView;

    @BindView(R.id.login)
    EditText mLoginView;

    @BindView(R.id.password)
    EditText mPasswordView;

    @BindView(R.id.login_form)
    View mLoginFormView;

    @BindView(R.id.error_form)
    View mErrorFormView;

    @BindView(R.id.sign_in)
    Button mButtonSignIn;

    // Widgets for connection error
    @BindView(R.id.progressBar)
    ProgressBar mProgressView;

    @BindView(R.id.tvError)
    TextView tvConnectionError;

    @BindView(R.id.btnTryAgain)
    Button btnTryAgain;

    /**
     * For input validation
     */
    private View focusView;
    private boolean cancel;

    /**
     * Regular expression for IP validation
     */
    private static final Pattern PARTIAL_IP_ADDRESS = Pattern.compile(
                    "^((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9])\\.){0,3}"+
                    "(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9])?$");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mIpView.addTextChangedListener(new TextWatcher() {
            private String mPreviousText = "";

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(PARTIAL_IP_ADDRESS.matcher(editable).matches()) {
                    mPreviousText = editable.toString();
                } else {
                    editable.replace(0, editable.length(), mPreviousText);
                }
            }
        });

        mButtonSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
        btnTryAgain.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                showWidgets(false);
                showConnectionError(false);
            }
        });
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        resetError(mIpView);
        resetError(mLoginView);
        resetError(mPasswordView);

        // Store values at the time of the login attempt.
        String ip = getEditTextInput(mIpView);
        String login = getEditTextInput(mLoginView);
        String password = getEditTextInput(mPasswordView);

        // Check for a valid IP
        checkRequiredField(mIpView, ip);

        // Check for a valid login.
        checkRequiredField(mLoginView, login);

        // Check for a valid password
        boolean isPasswordEmpty = TextUtils.isEmpty(password);
        boolean invalidPassword = !isPasswordValid(password);
        if (isPasswordEmpty || invalidPassword) {
            setError(mPasswordView,
                    isPasswordEmpty
                            ? R.string.error_field_required
                            : R.string.error_invalid_password);
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }   else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            signIn();
        }
    }

    //region Validation
    /**
     * Reset error for EditText
     * @param editText EditText widget
     */
    private void resetError(EditText editText) {
        editText.setError(null);
        cancel = false;
    }

    /**
     * Get user's input
     * @param editText EditText which input will be get
     * @return User's input
     */
    private String getEditTextInput(EditText editText) {
        return editText.getText().toString();
    }

    /**
     * Check if the user's input is empty and set error
     * @param editText EditText widget for checking
     * @param text User's input
     */
    private void checkRequiredField(EditText editText, String text) {
        if (TextUtils.isEmpty(text))
            setError(editText, R.string.error_field_required);
    }

    /**
     * Set error if the user's input is invalid
     * @param editText Edit text widget
     * @param errorResId String resource ID for an error message
     */
    private void setError(EditText editText, int errorResId) {
        editText.setError(getString(errorResId));
        focusView = editText;
        cancel = true;
    }

    /**
     * Check if the user's input for password is valid
     * @param password User's input for password
     * @return True if the password is valid
     */
    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }
    //endregion

    /**
     * Show correct layout which depends on loading state
     * @param isLoading Flag for loading state
     */
    private void showWidgets(boolean isLoading) {
        mLoginFormView.setVisibility(isLoading ? View.GONE : View.VISIBLE);
        mErrorFormView.setVisibility(isLoading? View.VISIBLE: View.GONE);
    }

    /**
     * Show correct widgets for concrete error state
     * @param isError Flag for error state
     */
    private void showConnectionError(boolean isError) {
        mProgressView.setVisibility(isError ? View.GONE : View.VISIBLE);
        tvConnectionError.setVisibility(isError ? View.VISIBLE : View.GONE);
        btnTryAgain.setVisibility(isError ? View.VISIBLE : View.GONE);
    }

    /**
     * Hide keyboard
     */
    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager manager =
                    (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (manager != null) {
                manager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    /**
     * Start sign in (emulate connection to server)
     */
    private void signIn() {
        hideKeyboard();
        showWidgets(true);
        mAuthTask = new UserLoginTask();
        mAuthTask.execute();
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            try {
                // Simulate network access.
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                return false;
            }

            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;

            if (success) {
                finish();
                showWidgets(false);
            } else {
                showConnectionError(true);
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showWidgets(false);
        }
    }
}

